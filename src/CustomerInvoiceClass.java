import com.devcamp.models.Customer;
import com.devcamp.models.Invoice;

public class CustomerInvoiceClass{
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Truong Tan Loc", 100000);
        System.out.println("Customer 1:");
        System.out.println(customer1);

        System.out.println("-----------------------------------------");
        Customer customer2 = new Customer(2, "Vuong Thi My Cam", 300000);
        System.out.println("Customer 2:");
        System.out.println(customer2);

        System.out.println("-----------------------------------------");

        Invoice invoice1 = new Invoice(1, customer1, 20000);
        System.out.println("Invoice 1:");
        System.out.println(invoice1);

        System.out.println("-----------------------------------------");

        Invoice invoice2 = new Invoice(2, customer2, 50000);
        System.out.println("Invoice 2:");
        System.out.println(invoice2);
    }
}
