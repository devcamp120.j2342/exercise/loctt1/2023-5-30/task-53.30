package com.devcamp.models;

public class Invoice {
     private int id;
     private Customer customer;
     private double amount;

     public Invoice() {
     }

     public Invoice(int id, Customer customer, double amount) {
          this.id = id;
          this.customer = customer;
          this.amount = amount;
     }

     public int getId() {
          return id;
     }

     public Customer getCustomer() {
          return customer;
     }

     public double getAmount() {
          return amount;
     }

     public void setCustomer(Customer customer) {
          this.customer = customer;
     }

     public void setAmount(double amount) {
          this.amount = amount;
     } 

     public int getCustomerID(){
          return 1;
     }

     public String getCustomerName(){
          return "Không có tên khách hàng nào!!!";
     }

     public int getCustomerDiscount(){
          return 0;
     }

     public double getAmountAfterDiscount(){
          return amount;
     }

     public String toString(){
          return "Invoice[id = " + this.id + ", " + this.customer + ", amount = " + this.amount + "]";
     }

     
}
